import time
from openerp.osv import fields, osv

from openerp.tools.translate import _

class skp_form_yearly_report(osv.osv_memory):
    _name = "skp.form.yearly.report"
    
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'print_date'       : fields.date('Tanggal Pembuatan',  required=True),
        'accepted_date'       : fields.date('Diterima Tanggal',  required=True),
        'validate_date'       : fields.date('Diterima Atasan Pejabat Penilai Tanggal',  required=True),
        'period_from_date'       : fields.date('Periode Awal Pembuatan',  required=True),
        'period_to_date'       : fields.date('Periode Akhir Pembuatan',  required=True),
        'user_id'        : fields.many2one('res.users', 'Pejabat Yang DInilai'),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        #'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        #'accepted_date': lambda *args: time.strftime('%Y-%m-%d'),
        #'validate_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    
    def print_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        if res:
            period_year = res['period_year']
            user_id = res['user_id'][0]
            print_month = res['print_date'][5:7]
            accept_month = res['accepted_date'][5:7]
            validate_month = res['validate_date'][5:7]
            #print print_month," ",accept_month," ",validate_month
            skp_yearly_pool = self.pool.get('skp.employee.yearly')
            skp_yearly_ids=skp_yearly_pool.search(cr, uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
            if print_month != '01' or accept_month != '01' or validate_month != '01':
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Tanggal Pembuatan dan Diterima Harus Dalam Periode Januari.'))
            if not skp_yearly_ids :
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Karena Belum Ada rekapitulasi Tahunan Pegawai.'))
        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_skp_form_yearly_report.report_skp_form_yearly_report', 
                        data=datas, context=context)
skp_form_yearly_report()