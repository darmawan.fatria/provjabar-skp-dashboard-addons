from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class skp_employee_yearly_history(osv.Model):
    _name = 'skp.employee.yearly.history'

    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        if isinstance(ids, (int, long)):
                    ids = [ids]
        reads = self.read(cr, uid, ids, ['job_id'], context=context)
        res = []
        for record in reads:

            if record['job_id']:
                name = record['job_id'][1]
            else :
                name ='-'

            res.append((record['id'], name))
        return res

    _columns = {
        'yearly_id': fields.many2one('skp.employee.yearly', 'Rekap',required=False,readonly=True),
        'company_id': fields.many2one('res.company', 'OPD',required=False,readonly=True),
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=False,readonly=True),
	    'user_id_atasan': fields.many2one('res.partner', 'Pejabat Penilai' ,domain="[('employee','=',True)]" ,readonly=False),
        'user_id_banding': fields.many2one('res.partner', 'Atasan Pejabat Penilai' ,domain="[('employee','=',True)]",readonly=False),
        'jml_skp_done': fields.integer(string='Jumlah Target SKP Yang Sudah Dinilai',  help="Jumlah SKP",readonly = True),
        'jml_skp': fields.integer(string='Jumlah Target SKP',  help="Jumlah SKP",readonly = True),
        'jml_realisasi_skp': fields.integer(string='Jumlah Realisasi SKP',  help="Jumlah SKP",readonly = True),
        'nilai_skp': fields.float( string='Nilai SKP',  help="Nilai SKP Akan Muncul Apabila Semua Kegiatan Dibulan Tertentu Sudah Selesai Dinilai", readonly = True),
        'nilai_skp_tambahan': fields.float( string='Nilai SKP+Tambahan+Kreatifitas',  help="Nilai SKP Akan Muncul Apabila Semua Kegiatan Dibulan Tertentu Sudah Selesai Dinilai", readonly = True),
        'nilai_skp_percent': fields.float( string='Nilai SKP(%)',  help="60% Dari Kegiatan DPA Biro, APBN, SOTK", readonly = True),
        'nilai_skp_tambahan_percent': fields.float( string='Nilai SKP+TB+Kreatifitas (%)',  help="60% Dari Kegiatan DPA Biro, APBN, SOTK Ditambah rata2 Tugas Tambahan Dan Nilai Kreatifitas", readonly = True),
        'fn_nilai_tambahan': fields.float( string='Nilai Tambahan',  help="Nilai Tambahan", readonly = True),
        'fn_nilai_kreatifitas': fields.float( string='Nilai Kreatifitas',help="Nilai Kreatifitas",readonly = True),
        'jumlah_perhitungan_skp': fields.float( string='Jumlah Perhitungan SKP',help="Jumlah Perhitungan SKP",readonly = True),
        'jml_tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'jml_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'indeks_nilai_skp' :fields.char('Indeks Nilai SKP',size=25,readonly=True),

        'emp_id': fields.related('yearly_id', 'employee_id', relation='res.partner', type='many2one', string='Pegawai', store=True,readonly=True),
        'emp_id_department_id': fields.related('emp_id', 'department_id', relation='partner.employee.department', type='many2one', string='Unit Kerja Pejabat Penilai', store=True,readonly=False),
        'emp_id_job_id': fields.related('emp_id', 'job_id', relation='partner.employee.job', type='many2one', string='Jabatan Pejabat Penilai', store=True,readonly=False),
        'emp_id_golongan_id': fields.related('emp_id', 'golongan_id', relation='partner.employee.golongan', type='many2one', string='Pangkat/Golongan Pejabat Penilai', store=True,readonly=False),
        'emp_id_company_id': fields.related('emp_id', 'company_id', relation='res.company', type='many2one', string='OPD Pejabat Penilai', store=True,readonly=False),


        'user_id_atasan_nip': fields.related('user_id_atasan','nip', type='char', string='NIP Pejabat Penilai', store=True,readonly=False),
        'user_id_atasan_department_id': fields.related('user_id_atasan', 'department_id', relation='partner.employee.department', type='many2one', string='Unit Kerja Pejabat Penilai', store=True,readonly=False),
        'user_id_atasan_job_id': fields.related('user_id_atasan', 'job_id', relation='partner.employee.job', type='many2one', string='Jabatan Pejabat Penilai', store=True,readonly=False),
        'user_id_atasan_golongan_id': fields.related('user_id_atasan', 'golongan_id', relation='partner.employee.golongan', type='many2one', string='Pangkat/Golongan Pejabat Penilai', store=True,readonly=False),
        'user_id_atasan_company_id': fields.related('user_id_atasan', 'company_id', relation='res.company', type='many2one', string='OPD Pejabat Penilai', store=True,readonly=False),

        'user_id_banding_nip': fields.related('user_id_banding','nip', type='char', string='NIP Atasan Pejabat Penilai', store=True,readonly=False),
        'user_id_banding_department_id': fields.related('user_id_banding', 'department_id', relation='partner.employee.department', type='many2one', string='Unit Kerja Atasan Pejabat Penilai', store=True,readonly=False),
        'user_id_banding_job_id': fields.related('user_id_banding', 'job_id', relation='partner.employee.job', type='many2one', string='Jabatan Atasan Pejabat Penilai', store=True,readonly=False),
        'user_id_banding_golongan_id': fields.related('user_id_banding', 'golongan_id', relation='partner.employee.golongan', type='many2one', string='Pangkat/Golongan Atasan Pejabat Penilai', store=True,readonly=False),
        'user_id_banding_company_id': fields.related('user_id_banding', 'company_id', relation='res.company', type='many2one', string='OPD Pejabat Penilai', store=True,readonly=False),

    }

skp_employee_yearly_history()

class skp_employee_yearly(osv.Model):
    _inherit = 'skp.employee.yearly'

    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        if isinstance(ids, (int, long)):
                    ids = [ids]
        reads = self.read(cr, uid, ids, ['target_period_year'], context=context)
        res = []
        for record in reads:
            name = record['target_period_year']

            res.append((record['id'], name))
        return res

    _columns = {
        'skp_ids': fields.one2many('skp.employee.yearly.history', 'yearly_id', 'Nilai SKP Dan Tambahan',order='create_date'),
    }

    def _get_akumulasi_realisasi_skp_tambahan_per_bulan(self, cr, user_id, employee_id,job_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False


        project_pool = self.pool.get('project.project')
        tambahan_pool = self.pool.get('project.tambahan.yearly')
        uid = SUPERUSER_ID
        data = {}
        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp']=0
        data['nilai_skp_tambahan']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_total']=0
        data['indeks_nilai_total']=''
        data['indeks_nilai_skp']=''
        data['jumlah_perhitungan_skp']=0

        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0



        #SKP=-----------------
        target_ids = project_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', 'in' , ('confirm','closed')),
                                                    ('job_id', '=', job_id.id),
                                                   ], context=None)
        count_skp_all=count_skp_done=0
        jumlah_total_perhitungan_skp=nilai_total_skp=0
        for target_obj in project_pool.read(cr, uid, target_ids, ['id','nilai_akhir','jumlah_perhitungan','state'
                                                                           ,'work_state'
                                                                           ], context=context) :
            if target_obj['work_state'] == 'done':
                count_skp_done+=1
                nilai_total_skp+=target_obj['nilai_akhir']
                jumlah_total_perhitungan_skp+=target_obj['jumlah_perhitungan']

            count_skp_all+=1

        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['jumlah_perhitungan_skp']=0
        data['nilai_skp']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan']=0
        data['nilai_skp_tambahan_percent']=0
        data['jml_tugas_tambahan']=0
        data['jml_kreatifitas']=0
        #data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)
        if count_skp_all > 0:
            data['jml_skp']=count_skp_all
            data['jml_skp_done']=count_skp_done
            data['jml_realisasi_skp']=count_skp_done
            if count_skp_done > 0:
                #Tugas Tambahan Dan Kreatifitas -------
                jml_tugas_tambahan=jml_kreatifitas=0
                nilai_tambahan_kreatifitas=fn_nilai_tambahan=fn_nilai_kreatifitas=0
                tambahan_ids = tambahan_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', '=' , 'done'),
                                                    ('job_id', '=', job_id.id),
                                                   ], context=None)


                for tambahan_obj in tambahan_pool.read(cr, uid, tambahan_ids, ['id','nilai_tambahan','nilai_kreatifitas',
                                                                       'nilai_akhir','jumlah_perhitungan','state',
                                                                        'realisasi_tugas_tambahan','realisasi_nilai_kreatifitas'
                                                                           ], context=context) :
                    fn_nilai_tambahan = tambahan_obj['nilai_tambahan']
                    fn_nilai_kreatifitas = tambahan_obj['nilai_kreatifitas']
                    jml_tugas_tambahan+=tambahan_obj['realisasi_tugas_tambahan']
                    jml_kreatifitas+=tambahan_obj['realisasi_nilai_kreatifitas']
                    nilai_tambahan_kreatifitas=tambahan_obj['nilai_akhir']

                data['jml_tugas_tambahan']=jml_tugas_tambahan
                data['jml_kreatifitas']=jml_kreatifitas
                data['fn_nilai_tambahan']=fn_nilai_tambahan
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas
                data['jumlah_perhitungan_skp']=round(nilai_total_skp,2)
                data['nilai_skp']=round(nilai_total_skp/count_skp_all,2)
                data['nilai_skp_percent']=round(data['nilai_skp']*0.6,2)
                data['nilai_skp_tambahan']=data['nilai_skp']+nilai_tambahan_kreatifitas
                data['nilai_skp_tambahan_percent']=round(data['nilai_skp_tambahan']*0.6,2)
                data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)


        data ['nilai_total'] = data['nilai_skp_tambahan_percent']
        data ['indeks_nilai_total'] = '-'#self.get_indeks_nilai(cr,uid,data ['nilai_total'],context=None)
        return data

skp_employee_yearly()