##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from mx import DateTime


class employee_mutation_popup(osv.Model):
    _name = 'employee.mutation.popup'
    _description ='Popup Mutasi Jabatan'

    def onchange_atasan_banding_nip(self, cr, uid, ids, user_id, context=None):
        res = {'value': {'user_id_banding_nip': False}}
        employee_pool = self.pool.get('res.partner')
        if user_id:
            user_obj = employee_pool.browse(cr, uid, [user_id,])
            res['value']['user_id_banding_nip'] = user_obj and user_obj[0] and user_obj[0].nip or False
        return res

    def onchange_atasan(self, cr, uid, ids, user_id_atasan, context=None):
        res = {'value': {'user_id_banding': False,
                         'user_id_atasan_nip': False
                         }}
        employee_pool = self.pool.get('res.partner')
        if user_id_atasan:
            user_obj = employee_pool.browse(cr, uid, [user_id_atasan,])
            res['value']['user_id_banding'] = user_obj and user_obj[0] and user_obj[0].user_id_atasan and user_obj[0].user_id_atasan.id or False
            res['value']['user_id_atasan_nip'] = user_obj and user_obj[0] and user_obj[0].nip  or False
        return res

    _columns = {
		'employee_id' : fields.many2one('res.partner', 'Pegawai',domain="[('employee','=',True)]"),
	    'name': fields.char("No SK", size=100,required=True ),
	    'period_year': fields.char("Tahun", size=4,required=True ),
        'company_id': fields.many2one('res.company', 'OPD',required=True,domain="[('employee_id_kepala_instansi','!=',False)]" ),
        'old_company_id': fields.many2one('res.company', 'OPD',required=True,domain="[('employee_id_kepala_instansi','!=',False)]" ),
        'old_job_id': fields.many2one('partner.employee.job', 'Jabatan',required=True),
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=True),
        'job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan',required=True),
        'eselon_id': fields.many2one('partner.employee.eselon', 'Eselon',required=False),
        'department_id': fields.many2one('partner.employee.department', 'Unit Kerja',required=True),
        'date': fields.date("TMT. Jabatan", required=True ),
	    'mutation' : fields.boolean('Mutasi'),

	    'user_id_atasan': fields.many2one('res.partner', 'Pejabat Penilai' ,domain="[('employee','=',True)]" ,required=True),
        'user_id_atasan_nip': fields.related('user_id_atasan','nip', type='char', string='NIP Pejabat Penilai', store=False,readonly=True),
        'user_id_banding': fields.many2one('res.partner', 'Atasan Pejabat Penilai' ,domain="[('employee','=',True)]",required=True),
        'user_id_banding_nip': fields.related('user_id_banding','nip', type='char', string='NIP Atasan Pejabat Penilai', store=False,readonly=True),
		'old_user_id_atasan': fields.many2one('res.partner', 'Pejabat Penilai' ,domain="[('employee','=',True)]" ,required=True),
		'old_user_id_banding': fields.many2one('res.partner', 'Atasan Pejabat Penilai' ,domain="[('employee','=',True)]",required=True),




	}
    def action_update_data_atasan(self, cr, uid, employee_obj,job_id,atasan_obj,banding_obj,company_obj,period_year, context=None):
	    perilaku_pool = self.pool.get('project.perilaku')
	    tambahan_kreatifitas_pool = self.pool.get('project.tambahan.kreatifitas')
	    employee_pool = self.pool.get('res.partner')
	    task_data = {}

	    task_domain = [('user_id','=',employee_obj.user_id.id),('target_period_year','=',period_year),('state','in',('draft','realisasi'))]

	    perilaku_ids = perilaku_pool.search(cr,uid,task_domain,context=None)
	    tambahan_kreatifitas_ids = tambahan_kreatifitas_pool.search(cr,uid,task_domain,context=None)
	    task_data.update({
                        'user_id_atasan':atasan_obj.user_id and atasan_obj.user_id.id or None,
		                'user_id_banding':banding_obj.user_id and banding_obj.user_id.id or None,
		                'user_id_bkd':company_obj.user_id_bkd and company_obj.user_id_bkd.id or None,
		                'company_id':company_obj.id,
		                'job_id':job_id.id
		    })
	    perilaku_pool.write(cr,SUPERUSER_ID,perilaku_ids,task_data,context=None)
	    tambahan_kreatifitas_pool.write(cr,SUPERUSER_ID,tambahan_kreatifitas_ids,task_data,context=None)

	    return True



    def action_close_target(self, cr, uid, employee_obj,period_year, context=None):
		target_pool = self.pool.get('project.project')
		target_domain = [('user_id','=',employee_obj.user_id.id),('target_period_year','=',period_year),('state','=','confirm')]

		target_to_close_ids = target_pool.search(cr,uid,target_domain,context=None)
		target_pool.set_closing_approve(cr,SUPERUSER_ID,target_to_close_ids,context=None)
		return True

    def action_create_yearly(self, cr, uid, new_job_property,period_year, context=None):
		yearly_pool = self.pool.get('skp.employee.yearly')
		skp_yearly_ids = yearly_pool.search(cr, uid, [('employee_id', '=', new_job_property.employee_id.id),
                                                      ('target_period_year', '=', period_year),
                                                   ], context=None)
		if not skp_yearly_ids:
			values = {
	                            'employee_id': new_job_property.employee_id.id,
								'company_id': new_job_property.company_id.id,
	                            'user_id': new_job_property.employee_id.user_id.id,
								'job_id':new_job_property.job_id.id,
								'user_id_atasan':new_job_property.user_id_atasan.id,
			                    'user_id_banding':new_job_property.user_id_banding.id,
	                            'target_period_year': period_year,
	                  }
			return yearly_pool.create(cr , uid, values, context=None)
		return False

    def action_create_tugas_tambahan(self, cr, uid, employee,period_year,job_id,employee_atasan,employee_banding,company_id, context=None):
		tb_pool = self.pool.get('project.tambahan.yearly')

		current_tb_ids = tb_pool.search(cr, uid, [('user_id', '=', employee.user_id.id),
		                                          ('job_id', '=', job_id.id),
                                                  ('target_period_year', '=', period_year),
                                                   ], context=None)

		if not current_tb_ids:
			values = {
	                            'state': 'draft',
	                            'user_id': employee.user_id.id,
								'job_id':job_id.id,
								'user_id_atasan':employee_atasan.user_id.id,
								'user_id_banding':employee_banding.user_id.id,
								'company_id':company_id.id,
								'use_target_for_calculation':False,
								'active':True,
			                    'name':'Tugas Tambahan ',
	                            'target_period_year': period_year,
	                  }
			return tb_pool.create(cr , uid, values, context=None)
		return False

    def action_change_job(self, cr, uid, ids, context=None):
        """  Mutasi
        """
        job_history_data = {}
        job_history_pool = self.pool.get('partner.employee.job.history')
        user_pool = self.pool.get('res.users')
        employee_pool = self.pool.get('res.partner')
        for new_job in self.browse(cr,uid,ids,context=None):
	        old_employee = new_job.employee_id
	        old_job_id = new_job.old_job_id
	        old_user_id_atasan = new_job.old_user_id_atasan
	        old_user_id_banding = new_job.old_user_id_banding
	        old_company_id = new_job.old_company_id
	        period_year=new_job.period_year
	        if new_job.mutation:

		        #create tb year for old job
		        self.action_create_tugas_tambahan(cr,uid,old_employee,period_year,old_job_id,old_user_id_atasan,old_user_id_banding,old_company_id,context=None)

	        job_history_data.update({
                                'partner_id':new_job.employee_id.id,
								'name':new_job.name,
								'company_name':new_job.company_id.name,
								'job_id_history':new_job.job_id.id,
								'job_type':new_job.job_type,
								'eselon_id':new_job.eselon_id.id,
								'department_id':new_job.department_id.id,
	                            'date' : new_job.date,
		                        'user_id_atasan':new_job.user_id_atasan and new_job.user_id_atasan.id or None,
								'user_id_banding':new_job.user_id_banding and new_job.user_id_banding.id or None,

	        })
	        job_history_pool.create(cr, SUPERUSER_ID, job_history_data,context)
	        tmt_date = DateTime.strptime(new_job.date,'%Y-%m-%d')




	        current_user_id = new_job.employee_id.user_id and new_job.employee_id.user_id.id  or False
	        if current_user_id:
		        company_ids = []
		        company_ids.append(new_job.company_id.id)
		        user_pool.write(cr,SUPERUSER_ID,current_user_id,
	                            {'company_id':new_job.company_id.id,
	                             'company_ids': [(6,0,company_ids)],
	                             }, context=None)
	        employee_pool.write(cr,SUPERUSER_ID,new_job.employee_id.id,{'employee':True,
	                                                                    'company_id':new_job.company_id.id,
	                                                                    'user_id_atasan':new_job.user_id_atasan and new_job.user_id_atasan.id or None,
														                'user_id_banding':new_job.user_id_banding and new_job.user_id_banding.id or None,
	                                                                    },context)

	        if new_job.mutation:

		        #create tb year for old job
		        self.action_create_tugas_tambahan(cr,uid,new_job.employee_id,period_year,new_job.job_id,new_job.user_id_atasan,new_job.user_id_banding,new_job.company_id,context=None)

		        self.action_close_target(cr,uid,new_job.employee_id,period_year,context=None)

		        self.action_create_yearly(cr,uid,new_job,period_year,context=None)



	        self.action_update_data_atasan(cr, uid, new_job.employee_id,new_job.job_id,new_job.user_id_atasan,new_job.user_id_banding,new_job.company_id,period_year, context=None)


		return True




employee_mutation_popup()