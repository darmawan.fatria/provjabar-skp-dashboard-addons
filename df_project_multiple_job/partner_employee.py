# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from openerp.tools.translate import _
from mx import DateTime


class res_partner(osv.Model):
    _inherit = 'res.partner'

    def action_change_jabatan(self, cr, uid, ids, context=None):
        #print "ubah jabatan"
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_multiple_job', 'action_employee_mutation_popup_form_view')
        employee_obj = self.browse(cr, uid, ids, context=None)[0]
        now=DateTime.today();
        period_year =  now.strftime('%Y')
        #print "employee id ",employee_obj.name
        return {
							'name':_("Silahkan pilih jabatan baru"),
							'view_mode': 'form',
							'view_id': view_id,
							'view_type': 'form',
							'res_model': 'employee.mutation.popup',
							'type': 'ir.actions.act_window',
							'nodestroy': True,
							'target': 'new',
							'domain': '[]',
							'context': {
							    'default_employee_id': employee_obj.id,
								'default_mutation':False,
								'default_period_year':period_year

							}
		}
    def action_do_mutation(self, cr, uid, ids, context=None):

        #print "action_do_mutation jabatan"
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_multiple_job', 'action_employee_mutation_popup_form_view')
        employee_obj = self.browse(cr, uid, ids, context=None)[0]
        old_job_id = employee_obj.job_id
        #print "employee id ",employee_obj.name
        #print "old_job_id id ",old_job_id.name
        now=DateTime.today();
        period_year =  now.strftime('%Y')
        return {
							'name':_("Silahkan pilih jabatan baru"),
							'view_mode': 'form',
							'view_id': view_id,
							'view_type': 'form',
							'res_model': 'employee.mutation.popup',
							'type': 'ir.actions.act_window',
							'nodestroy': True,
							'target': 'new',
							'domain': '[]',
							'context': {
							    'default_employee_id': employee_obj.id,
								'default_mutation':True,
								'default_period_year':period_year,
								'default_old_job_id':old_job_id.id,
								'default_old_user_id_atasan':employee_obj.user_id_atasan.id,
								'default_old_user_id_banding':employee_obj.user_id_banding.id,
								'default_old_company_id':employee_obj.company_id.id,
							}
		}



res_partner()
class partner_employee_job_history(osv.Model):
    _inherit = "partner.employee.job.history"
    _columns = {
	    'user_id_atasan': fields.many2one('res.partner', 'Pejabat Penilai' ,domain="[('employee','=',True)]" ),
	    'user_id_banding': fields.many2one('res.partner', 'Atasan Pejabat Penilai' ,domain="[('employee','=',True)]"),
	}
partner_employee_job_history()