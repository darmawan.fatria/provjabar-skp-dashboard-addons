# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from datetime import datetime,timedelta
import time
from mx import DateTime


class project(osv.Model):
    _inherit = 'project.project'

    def create(self, cr, uid, vals, context=None):

        if vals.get('user_id', False) :
            user_id = vals.get('user_id', False)
            user_pool = self.pool.get('res.users')
            user_obj = user_pool.browse(cr,SUPERUSER_ID,user_id,context=None)
            vals.update({'job_id': user_obj.partner_id and user_obj.partner_id.job_id and user_obj.partner_id.job_id.id or None })

        return super(project, self).create(cr, uid, vals, context)

    def set_evaluated_yearly(self, cr, uid, ids, context=None):

        for task_id in ids :
            self.write(cr, uid, task_id, {'target_realiasi_notsame':self.is_status_target_realiasi_notsame(cr, uid, task_id, context),
                                      'biaya_verifikasi_notsame':self.is_status_biaya_verifikasi_notsame(cr, uid, task_id, context),
                                    }, context=context)
        self.write(cr, uid, ids, {'work_state':'evaluated', },)
        for target_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,uid,target_tahunan.user_id,target_tahunan.employee_id,target_tahunan.job_id,target_tahunan.target_period_year)

        return True

    def set_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'work_state':'done'}, context=context)
        for target_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,uid,target_tahunan.user_id,target_tahunan.employee_id,target_tahunan.job_id,target_tahunan.target_period_year)


    _columns = {
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=False),
        'work_state': fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                        ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                        ('appeal', 'Banding'), ('evaluated', 'BKD'), ('rejected_bkd', 'Pengajuan Ditolak BKD'),
                                        ('propose_to_close', 'Pengajuan Closing Target'), ('closed', 'Closed'),
                                        ('propose_correction', 'Pengajuan Revisi'), ('correction', 'Revisi Target'),
                                        ('done', 'Selesai'), ('cancelled', 'Cancel')], 'Status Pekerjaan'),
    }

    def set_propose_correction_yearly(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'propose_correction'}, context=context)
    def set_reject_correction_yearly(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'realisasi'}, context=context)
    def set_approve_correction_yearly(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'correction'}, context=context)
    def set_realisasi_yearly(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'realisasi'}, context=context)

    def action_propose_correction_yearly(self, cr, uid, ids, context=None):
        """ Ajukan Revisi ke bkd,
        """
        if self.is_user_realiasi(cr, uid, ids, context) :
            return self.set_propose_correction_yearly(cr, uid, ids, context=context)
    def action_approve_correction_yearly(self, cr, uid, ids, context=None):
        """ pengajuan koreksi diterima, balik lagi ke user
        """
        if self.is_verificator_realiasi(cr, uid, ids, context) :
            return self.set_approve_correction_yearly(cr, uid, ids, context=context)
    def action_reject_correction_yearly(self, cr, uid, ids, context=None):
        """ pengajuan koresi target di tolak balik lagi ke user
        """
        if self.is_verificator_realiasi(cr, uid, ids, context) :
            return self.set_reject_correction_yearly(cr, uid, ids, context=context)
    def action_create_realisasi_yearly(self, cr, uid, ids, context=None):
        """ Isi Realisasi
        """
        if self.is_user_realiasi(cr, uid, ids, context) :
            return self.set_realisasi_yearly(cr, uid, ids, context=context)

    def do_skp_summary_calculation(self, cr,uid, user_id,employee_id,job_id, target_period_year,  context=None):

        sey_pool = self.pool.get('skp.employee.yearly')
        sey_detail_pool = self.pool.get('skp.employee.yearly.history')
        data_skp_summary  = sey_pool._get_akumulasi_realisasi_skp_tambahan_per_bulan(cr, user_id,employee_id,job_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                    ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)


        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_id in skp_yearly_ids:
                yearly_detail_ids = sey_detail_pool.search(cr, uid, [('yearly_id', '=', skp_yearly_id),
                                                                    ('job_id', '=', job_id.id),
                                                   ], context=None)
                if not yearly_detail_ids:
                        for job_obj in employee_id.job_id_history:
                            if job_obj.job_id_history.id == job_id.id :
                                atasan = job_obj.user_id_atasan or employee_id.user_id_atasan
                                banding = job_obj.user_id_banding or employee_id.user_id_banding
                                company = job_obj.department_id and job_obj.department_id.company_id or employee_id.company_id
                                detail_values = {
                                    'yearly_id': skp_yearly_id,
                                    'job_id': job_id.id,
                                    'user_id_atasan': atasan.id,
                                    'user_id_banding': banding.id,
                                    'company_id':company.id
                                }
                                #print "create new data : ",detail_values
                                new_skp_yearly_detail_id=sey_detail_pool.create(cr , uid, detail_values, context=None)

            data_detail = {}
            data_detail['jml_skp']=0
            data_detail['jml_skp_done']=0
            data_detail['jml_realisasi_skp']=0
            data_detail['nilai_perilaku_percent']=0
            data_detail['nilai_skp_percent']=0
            data_detail['nilai_skp_tambahan_percent']=0
            data_detail['nilai_skp']=0
            data_detail['nilai_skp_tambahan']=0
            data_detail['fn_nilai_tambahan']=0
            data_detail['fn_nilai_kreatifitas']=0
            data_detail['jml_tugas_tambahan']=0
            data_detail['jml_kreatifitas']=0
            data_detail['indeks_nilai_skp']=''
            data_detail['jumlah_perhitungan_skp']=0
            total_detail=0;
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                nilai_tambahan=0
                nilai_kreatifitas=0
                jml_tugas_tambahan=0
                for detail_obj in skp_yearly_obj.skp_ids:
                        if detail_obj.job_id.id == job_id.id :
                            update_detail_values = {
                                'jml_skp': data_skp_summary['jml_skp'],
                                'jml_skp_done': data_skp_summary['jml_skp_done'],
                                'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'jumlah_perhitungan_skp': data_skp_summary['jumlah_perhitungan_skp']   ,
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'indeks_nilai_skp': data_skp_summary['indeks_nilai_skp'],
                            }
                            sey_detail_pool.write(cr , uid,[detail_obj.id,], update_detail_values, context=None)
                            data_detail['jml_skp']=data_detail['jml_skp']+data_skp_summary['jml_skp']
                            data_detail['jml_skp_done']=data_detail['jml_skp_done']+data_skp_summary['jml_skp_done']
                            data_detail['jumlah_perhitungan_skp']=data_detail['jumlah_perhitungan_skp']+data_skp_summary['jumlah_perhitungan_skp']
                            data_detail['jml_realisasi_skp']=data_detail['jml_realisasi_skp']+data_skp_summary['jml_realisasi_skp']
                            data_detail['nilai_skp']=data_detail['nilai_skp']+data_skp_summary['nilai_skp']
                            data_detail['nilai_skp_tambahan']=data_detail['nilai_skp_tambahan']+data_skp_summary['nilai_skp_tambahan']
                            data_detail['nilai_skp_percent']=data_detail['nilai_skp_percent']+data_skp_summary['nilai_skp_percent']
                            data_detail['nilai_skp_tambahan_percent']=data_detail['nilai_skp_tambahan_percent']+data_skp_summary['nilai_skp_tambahan_percent']

                            if data_skp_summary['fn_nilai_kreatifitas'] >= nilai_kreatifitas:
                                nilai_kreatifitas = data_skp_summary['fn_nilai_kreatifitas']
                            jml_tugas_tambahan += data_skp_summary['jml_tugas_tambahan']
                        else :

                            data_detail['jml_skp']=data_detail['jml_skp']+detail_obj.jml_skp
                            data_detail['jml_skp_done']=data_detail['jml_skp_done']+detail_obj.jml_skp_done
                            data_detail['jumlah_perhitungan_skp']=data_detail['jumlah_perhitungan_skp']+detail_obj.jumlah_perhitungan_skp
                            data_detail['jml_realisasi_skp']=data_detail['jml_realisasi_skp']+detail_obj.jml_realisasi_skp
                            data_detail['nilai_skp']=data_detail['nilai_skp']+detail_obj.nilai_skp
                            data_detail['nilai_skp_tambahan']=data_detail['nilai_skp_tambahan']+detail_obj.nilai_skp_tambahan
                            data_detail['nilai_skp_percent']=data_detail['nilai_skp_percent']+detail_obj.nilai_skp_percent
                            data_detail['nilai_skp_tambahan_percent']=data_detail['nilai_skp_tambahan_percent']+detail_obj.nilai_skp_tambahan_percent

                            if detail_obj.fn_nilai_kreatifitas >= nilai_kreatifitas:
                                nilai_kreatifitas = data_skp_summary['fn_nilai_kreatifitas']
                            jml_tugas_tambahan += detail_obj.jml_tugas_tambahan

                        total_detail=total_detail+1

                if total_detail > 0 :
                    #calculate tambahan
                    final_nilai_tambahan = jml_tugas_tambahan
                    if final_nilai_tambahan >= 1 :
                        lookup_nilai_pool = self.pool.get('acuan.penilaian')

                        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_nilai', '=', 'threshold'), ('type', '=', 'tugas_tambahan')
                                                                            , ('nilai_bawah', '<=', final_nilai_tambahan), ('nilai_atas', '>=', final_nilai_tambahan)], context=None)
                        try :
                            lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                            nilai_tambahan = lookup_nilai.nilai_tunggal
                            data_detail['fn_nilai_tambahan'] = nilai_tambahan
                        except :
                            print "error calculate tambahan"

                    nilai_skp_akumulasi = (data_detail['nilai_skp']/total_detail)
                    nilai_skp_tambahan_akumulasi =nilai_skp_akumulasi + nilai_tambahan + nilai_kreatifitas
                    nilai_skp_akumulasi_percent = round(nilai_skp_akumulasi*0.6,2)
                    indeks_nilai_skp_akumulasi = sey_pool.get_indeks_nilai(cr,uid,nilai_skp_akumulasi,context=None)
                    nilai_skp_tambahan_akumulasi_percent = round(nilai_skp_tambahan_akumulasi*0.6,2)
                    nilai_total = skp_yearly_obj.nilai_perilaku_percent + ( nilai_skp_tambahan_akumulasi_percent )
                    indeks_nilai_total = sey_pool.get_indeks_nilai(cr,uid,nilai_total,context=None)

                    update_values = {
                                    'jml_skp': data_detail['jml_skp'],
                                    'jml_skp_done': data_detail['jml_skp_done'],
                                    'jumlah_perhitungan_skp': data_detail['jumlah_perhitungan_skp'],
                                    'jml_realisasi_skp':data_detail['jml_realisasi_skp'],
                                    'nilai_skp': nilai_skp_akumulasi ,
                                    'nilai_skp_tambahan': nilai_skp_tambahan_akumulasi  ,
                                    'nilai_skp_percent': nilai_skp_akumulasi_percent,
                                    'nilai_skp_tambahan_percent': nilai_skp_tambahan_akumulasi_percent,
                                    'indeks_nilai_skp': indeks_nilai_skp_akumulasi,
                                    'indeks_nilai_total': indeks_nilai_total,
                                    'nilai_total': round(nilai_total,2),
                                }
                    sey_pool.write(cr , uid,[skp_yearly_obj.id,], update_values, context=None)
        return True;

project()

