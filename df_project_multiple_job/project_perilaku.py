# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from datetime import datetime,timedelta
import time
from mx import DateTime


class project_perilaku(osv.Model):
    _inherit = 'project.perilaku'

    def create(self, cr, uid, vals, context=None):

        if vals.get('user_id', False) :
            user_id = vals.get('user_id', False)
            user_pool = self.pool.get('res.users')
            user_obj = user_pool.browse(cr,SUPERUSER_ID,user_id,context=None)
            vals.update({'job_id': user_obj.partner_id and user_obj.partner_id.job_id and user_obj.partner_id.job_id.id or None })

        return super(project_perilaku, self).create(cr, uid, vals, context)
    _columns = {
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=False),
    }

    def do_monthly_perilaku_summary_calculation(self, cr, uid, ids,sign,  context=None):
        """ BKD->Done (Rekap Summary) """
        # Summary Calculation

        #print "start  start monthly calc"
        skp_employee_pool = self.pool.get('skp.employee')
        if not isinstance(ids, list): ids = [ids]

        for task_obj in self.browse(cr, uid, ids, context=context):

                #print "start  search skp emp"
                domain= task_obj.employee_id.id,task_obj.target_period_year,task_obj.target_period_month
                query = """ SELECT  id,coalesce(nilai_skp_percent,0),coalesce(nilai_skp_tambahan_percent,0)
                        FROM SKP_EMPLOYEE WHERE employee_id = %s
                        AND target_period_year = %s
                        AND target_period_month = %s
                                """
                cr.execute(query,domain)
                rs_skp_emp = cr.fetchall()
                #print "end  search skp emp"


                if not rs_skp_emp:
                        company_id = task_obj.employee_id.company_id and task_obj.employee_id.company_id.id or None
                        biro_id = task_obj.employee_id.biro_id and task_obj.employee_id.biro_id.id or None
                        job_id = task_obj.job_id and task_obj.job_id.id or None
                        department_id = task_obj.employee_id.department_id and task_obj.employee_id.department_id.id or None
                        is_kepala_opd = task_obj.employee_id.is_kepala_opd

                        self.pool.get('skp.employee').insert_skp_employee_query(cr,task_obj.employee_id.id,task_obj.user_id.id,task_obj.target_period_year,
                                                                                task_obj.target_period_month,company_id,department_id,
                                                                                biro_id,is_kepala_opd,job_id)

                        cr.execute(query,domain)
                        rs_skp_emp = cr.fetchall()

                #print "stat main calc  search skp emp"
                for skp_emp_id,nilai_skp_percent,nilai_skp_tambahan_percent in rs_skp_emp:
                        if sign > 0 :
                            jml_perilaku =1
                            nilai_perilaku =task_obj.nilai_akhir
                        else :
                            jml_perilaku =0
                            nilai_perilaku =0

                        nilai_perilaku_percent =  nilai_perilaku * 0.4
                        nilai_total = nilai_skp_tambahan_percent+nilai_perilaku_percent
                        nilai_tpp=0

                        #print "start do write (manual query)"
                        #skp_employee_pool.write(cr , uid,skp_emp_id, update_values, context=None)
                        update_query = """ UPDATE SKP_EMPLOYEE
                        SET jml_perilaku = %s,
                        nilai_perilaku = %s,
                        nilai_perilaku_percent = %s,
                        nilai_pelayanan = %s,
                        nilai_integritas = %s,
                        nilai_komitmen = %s,
                        nilai_disiplin = %s,
                        nilai_kerjasama = %s,
                        nilai_kepemimpinan = %s,
                        nilai_total = %s,
                        nilai_tpp = %s
                        where id = %s

                                """
                        cr.execute(update_query, (jml_perilaku, nilai_perilaku,nilai_perilaku_percent,
                                                task_obj.nilai_pelayanan,task_obj.nilai_integritas,
                                                 task_obj.nilai_komitmen,task_obj.nilai_disiplin, task_obj.nilai_kerjasama,
                                                 task_obj.nilai_kepemimpinan,nilai_total,nilai_tpp ,
                                                  skp_emp_id))
                        #print "end do write"
                #kalkulasi akumulasi perilaku tahunan
                #print "end main calc  search skp emp"

                #print "start  do_perilaku_summary_calculation"
                self.do_perilaku_summary_calculation(cr,uid,task_obj.user_id,task_obj.employee_id,task_obj.target_period_year)
                #print "end  do_perilaku_summary_calculation"


        #print "end  start monthly calc"
        return True;

project_perilaku()

