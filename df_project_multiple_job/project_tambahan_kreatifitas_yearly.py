from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from openerp.tools.translate import _



class project_tambahan_kreatifitas_yearly(osv.Model):
    _inherit = 'project.tambahan.yearly'

    def create(self, cr, uid, vals, context=None):

        if vals.get('target_period_year', False) :
            period_year = vals.get('target_period_year', False)
            job_id = vals.get('job_id', False)
            current_tb_id = self.search(cr, uid, [('user_id', '=', uid),('job_id', '=', job_id), ('target_period_year', '=', period_year), ('state', '!=', 'cancelled')], context=None)


            if current_tb_id:
                raise osv.except_osv(_('Invalid Action!'),
                                             _('Tidak Boleh Membuat Tugas Tambahan Dan Kreatifitas Dalam Jabatan Yang Sama Lebih Dari 1 Kali.'))
        return super(project_tambahan_kreatifitas_yearly, self).create(cr, uid, vals, context)

    _columns = {
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=False),
    }

    # init field
    def init_field(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task = {}
        user_pool = self.pool.get('res.users')
        for task_generate in self.browse(cr, uid, ids, context=context):
            # check Duplicate
            # Init Field
            user_id = task_generate.user_id.id
            user_obj = task_generate.user_id
            target_category = 'bulanan'
            description = ''

            target_period_year = task_generate.target_period_year
            company_id = None
            currency_id = None
            user_id_bkd = None
            employee = user_obj.partner_id
            if user_id != uid:
                raise osv.except_osv(_('Invalid Action!'),
                                     _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee:
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                     _(
                                         'Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else:
                company = employee.company_id
                company_id = company.id
                currency_id = employee.company_id.currency_id.id

                # print "company_id : ",company_id,' - ',currency_id

                if not company_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Unit Dinas Pegawai Belum Dilengkapi.'))
                # print "employee parent : ",employee.parent_id
                if not task_generate.user_id_bkd:
                    if not company.user_id_bkd:
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                             _(
                                                 'Staff Pemeriksa Dari BKD Tidak Tersedia Untuk Unit Anda, Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else:
                        user_id_bkd = company.user_id_bkd.id
                else:
                    user_id_bkd = task_generate.user_id_bkd.id
                if not employee.user_id_atasan:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _('Proses Tidak Dapat Dilanjutkan Karena Data Pejabat Penilai Belum Terisi.'))
                if not employee.user_id_banding:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _('Proses Tidak Dapat Dilanjutkan Karena Data Pejabat Pengajuan Banding.'))
                if not employee.job_type or not employee.job_id or not employee.golongan_id:
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                         _(
                                             'Proses Tidak Dapat Dilanjutkan Karena Data Jabatan Dan Pangkat Belum Diiisi.'))
            user_id_atasan = task_generate.user_id_atasan.id
            user_id_banding = task_generate.user_id_banding.id

            if not task_generate.user_id_atasan.id:
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not task_generate.user_id_banding.id:
                user_id_banding = employee.user_id_banding.user_id.id

            task.update({
                'company_id': company_id,
                'user_id_atasan': user_id_atasan or False,
                'user_id_banding': user_id_banding or False,
                'user_id_bkd': user_id_bkd or False,
                'currency_id': currency_id,
                'active': True,
                'job_id': employee.job_id.id
            })
            self.write(cr, uid, [task_generate.id], task, context)

        return True
    def set_evaluated(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'evaluated'}, context=context)
        for tb_kr_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,SUPERUSER_ID,tb_kr_tahunan.user_id,tb_kr_tahunan.employee_id,tb_kr_tahunan.job_id,tb_kr_tahunan.target_period_year)
        return True
    def set_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'done'}, context=context)
        for tb_kr_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,uid,tb_kr_tahunan.user_id,tb_kr_tahunan.employee_id,tb_kr_tahunan.job_id,tb_kr_tahunan.target_period_year)
        return True

    def do_skp_summary_calculation(self, cr,uid, user_id,employee_id, job_id,target_period_year,  context=None):

        sey_pool = self.pool.get('skp.employee.yearly')
        sey_detail_pool = self.pool.get('skp.employee.yearly.history')
        data_skp_summary  = sey_pool._get_akumulasi_realisasi_skp_tambahan_per_bulan(cr, user_id,employee_id,job_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)

        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_id in skp_yearly_ids:
                yearly_detail_ids = sey_detail_pool.search(cr, uid, [('yearly_id', '=', skp_yearly_id),
                                                                    ('job_id', '=', job_id.id),
                                                   ], context=None)
                if not yearly_detail_ids:
                        for job_obj in employee_id.job_id_history:
                            if job_obj.job_id_history.id == job_id.id :
                                atasan = job_obj.user_id_atasan or employee_id.user_id_atasan
                                banding = job_obj.user_id_banding or employee_id.user_id_banding
                                company = job_obj.department_id and job_obj.department_id.company_id or employee_id.company_id
                                detail_values = {
                                    'yearly_id': skp_yearly_id,
                                    'job_id': job_id.id,
                                    'user_id_atasan': atasan.id,
                                    'user_id_banding': banding.id,
                                    'company_id':company.id
                                }
                                new_skp_yearly_detail_id=sey_detail_pool.create(cr , uid, detail_values, context=None)

            data_detail = {}
            data_detail['nilai_perilaku_percent']=0
            data_detail['nilai_skp_percent']=0
            data_detail['nilai_skp_tambahan_percent']=0
            data_detail['nilai_skp']=0
            data_detail['nilai_skp_tambahan']=0
            data_detail['fn_nilai_tambahan']=0
            data_detail['fn_nilai_kreatifitas']=0
            data_detail['jml_tugas_tambahan']=0
            data_detail['jml_kreatifitas']=0
            data_detail['indeks_nilai_skp']=''
            data_detail['jumlah_perhitungan_skp']=0
            total_detail=0;
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                count_of_month = 1#data_skp_summary['count_of_month']
                nilai_tambahan=0
                nilai_kreatifitas=0
                jml_tugas_tambahan=0
                for detail_obj in skp_yearly_obj.skp_ids:
                        if detail_obj.job_id.id == job_id.id :

                            update_detail_values = {
                                'fn_nilai_tambahan': data_skp_summary['fn_nilai_tambahan'],
                                'fn_nilai_kreatifitas': data_skp_summary['fn_nilai_kreatifitas'],
                                'jml_tugas_tambahan' :data_skp_summary['jml_tugas_tambahan'],
                                 'jml_kreatifitas':data_skp_summary['jml_kreatifitas'],

                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'indeks_nilai_skp': data_skp_summary['indeks_nilai_skp'],
                            }
                            sey_detail_pool.write(cr , uid,[detail_obj.id,], update_detail_values, context=None)
                            data_detail['fn_nilai_tambahan']=data_detail['fn_nilai_tambahan']+data_skp_summary['fn_nilai_tambahan']
                            data_detail['fn_nilai_kreatifitas']=data_detail['fn_nilai_kreatifitas']+data_skp_summary['fn_nilai_kreatifitas']
                            data_detail['nilai_skp']=data_detail['nilai_skp']+data_skp_summary['nilai_skp']
                            data_detail['nilai_skp_tambahan']=data_detail['nilai_skp_tambahan']+data_skp_summary['nilai_skp_tambahan']
                            data_detail['nilai_skp_percent']=data_detail['nilai_skp_percent']+data_skp_summary['nilai_skp_percent']
                            data_detail['nilai_skp_tambahan_percent']=data_detail['nilai_skp_tambahan_percent']+data_skp_summary['nilai_skp_tambahan_percent']

                            if data_skp_summary['fn_nilai_kreatifitas'] >= nilai_kreatifitas:
                                nilai_kreatifitas = data_skp_summary['fn_nilai_kreatifitas']
                            jml_tugas_tambahan += data_skp_summary['jml_tugas_tambahan']
                        else :

                            data_detail['fn_nilai_tambahan']=data_detail['fn_nilai_tambahan']+detail_obj.fn_nilai_tambahan
                            data_detail['fn_nilai_kreatifitas']=data_detail['fn_nilai_kreatifitas']+detail_obj.fn_nilai_kreatifitas
                            data_detail['nilai_skp']=data_detail['nilai_skp']+detail_obj.nilai_skp
                            data_detail['nilai_skp_tambahan']=data_detail['nilai_skp_tambahan']+detail_obj.nilai_skp_tambahan
                            data_detail['nilai_skp_percent']=data_detail['nilai_skp_percent']+detail_obj.nilai_skp_percent
                            data_detail['nilai_skp_tambahan_percent']=data_detail['nilai_skp_tambahan_percent']+detail_obj.nilai_skp_tambahan_percent

                            if detail_obj.fn_nilai_kreatifitas >= nilai_kreatifitas:
                                nilai_kreatifitas = data_skp_summary['fn_nilai_kreatifitas']
                            jml_tugas_tambahan += detail_obj.jml_tugas_tambahan

                        total_detail=total_detail+1

                if total_detail > 0 :
                    #calculate tambahan
                    final_nilai_tambahan = jml_tugas_tambahan
                    if final_nilai_tambahan >= 1 :
                        lookup_nilai_pool = self.pool.get('acuan.penilaian')

                        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_nilai', '=', 'threshold'), ('type', '=', 'tugas_tambahan')
                                                                            , ('nilai_bawah', '<=', final_nilai_tambahan), ('nilai_atas', '>=', final_nilai_tambahan)], context=None)
                        try :
                            lookup_nilai = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
                            nilai_tambahan = lookup_nilai.nilai_tunggal
                            data_detail['fn_nilai_tambahan'] = nilai_tambahan
                        except :
                            print "error calculate tambahan"

                    nilai_skp_akumulasi = (data_detail['nilai_skp']/total_detail)
                    nilai_skp_tambahan_akumulasi =nilai_skp_akumulasi + nilai_tambahan + nilai_kreatifitas
                    nilai_skp_akumulasi_percent = round(nilai_skp_akumulasi*0.6,2)
                    indeks_nilai_skp_akumulasi = sey_pool.get_indeks_nilai(cr,uid,nilai_skp_akumulasi,context=None)
                    nilai_skp_tambahan_akumulasi_percent = round(nilai_skp_tambahan_akumulasi*0.6,2)
                    nilai_total = skp_yearly_obj.nilai_perilaku_percent + ( nilai_skp_tambahan_akumulasi_percent )
                    indeks_nilai_total = sey_pool.get_indeks_nilai(cr,uid,nilai_total,context=None)

                    update_values = {
                                    'fn_nilai_tambahan': data_detail['fn_nilai_tambahan'],
                                    'fn_nilai_kreatifitas': data_detail['fn_nilai_kreatifitas'],
                                    'nilai_skp': nilai_skp_akumulasi ,
                                    'nilai_skp_tambahan': nilai_skp_tambahan_akumulasi  ,
                                    'nilai_skp_percent': nilai_skp_akumulasi_percent,
                                    'nilai_skp_tambahan_percent': nilai_skp_tambahan_akumulasi_percent,
                                    'indeks_nilai_skp': indeks_nilai_skp_akumulasi,
                                    'indeks_nilai_total': indeks_nilai_total,
                                    'nilai_total': round(nilai_total,2),
                                }
                    sey_pool.write(cr , uid,[skp_yearly_obj.id,], update_values, context=None)
        return True;




project_tambahan_kreatifitas_yearly()