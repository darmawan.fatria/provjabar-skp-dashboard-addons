# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from datetime import datetime,timedelta
import time
from mx import DateTime


class skp_employee(osv.Model):
    _inherit = 'skp.employee'

    def insert_skp_employee_query(self, cr, employee_id,user_id,target_period_year,target_period_month,company_id,department_id,biro_id,is_kepala_opd,job_id):
        insert_query = """ INSERT INTO skp_employee(employee_id,user_id,target_period_year,target_period_month
                        ,company_id,department_id,biro_id,is_kepala_opd,job_id)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)

                                """
        cr.execute(insert_query, (employee_id,user_id,target_period_year,target_period_month,company_id,department_id,biro_id,is_kepala_opd,job_id))


    _columns = {
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',readonly=True),
    }

skp_employee()

