##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Lampiran Penilaian Prestasi Kerja Versi BKN",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "SKP/Reporting",
    "description": """
    Lampiran Penilaian Prestasi Kerja Versi BKN
    """,
    "website" : "http://www.mediasee.net",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                "df_skp_employee",
                "df_skp_form_yearly_report",
                ],
    'data': [
                   "skp_target_yearly_report_view.xml",
                   "view/report_skp_target_yearly_report_view.xml",
                    "view/report_skp_target_realisasi_yearly_report_view.xml",
                   ],
    'active': False,
    'installable': True
    
}
