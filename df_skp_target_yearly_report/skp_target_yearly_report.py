import time
from openerp.osv import fields, osv
from openerp.tools.translate import _

class skp_target_yearly_report(osv.osv_memory):
    _name = "skp.target.yearly.report"
    
    def onchange_yearly(self, cr, uid, ids, template_jabatan_id, context=None):
        res = {'value': {
                        'skp_job_id': False,

                        }
                }

        return res

    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=False),
        'period_from_date'       : fields.date('Periode Awal Pembuatan',  required=True),
        'period_to_date'       : fields.date('Periode Akhir Pembuatan',  required=True),
        'print_date'       : fields.date('Tanggal Pembuatan',  required=True),
        'user_id'        : fields.many2one('res.users', 'Pejabat Yang Dinilai'),
        'break_target': fields.integer('Jeda Baris TTD Target', required=True),
        'break_realisasi': fields.integer('Jeda Baris TTD Realisasi', required=True),
        'yearly_id': fields.many2one('skp.employee.yearly', 'Periode',required=False),
        'skp_job_id': fields.many2one('skp.employee.yearly.history', 'Jabatan',required=True),
        'city'       : fields.char('Tempat', size=28, required=True),
        'ttd_date'       : fields.date('Tanggal Diterima',  required=True),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        #'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
        'skp_job_id':[]
    }
    
    def print_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        if res:

            period_year =  res['yearly_id'][1]
            user_id = res['user_id'][0]
            res['period_year'] = period_year

            skp_yearly_pool = self.pool.get('skp.employee.yearly')
            skp_yearly_ids=skp_yearly_pool.search(cr, uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
            if not skp_yearly_ids :
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Karena Belum Ada rekapitulasi Tahunan Pegawai.'))

        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_skp_target_yearly_report.report_skp_target_yearly_report', 
                        data=datas, context=context)
    def print_realisasi_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        if res:
            print "yearly : ",res['yearly_id']
            print "skp_job_id : ",res['skp_job_id']
            period_year =  res['yearly_id'][1]
            user_id = res['user_id'][0]
            res['period_year'] = period_year

            skp_yearly_pool = self.pool.get('skp.employee.yearly')
            skp_yearly_ids=skp_yearly_pool.search(cr, uid, [('target_period_year','=',period_year),('user_id','=',user_id)], context=None)
            if not skp_yearly_ids :
                raise osv.except_osv(_('Laporan Tidak Bisa Di Download'),
                                        _('Karena Belum Ada rekapitulasi Tahunan Pegawai.'))

        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids,
                        'df_skp_target_yearly_report.report_skp_target_realisasi_yearly_report',
                        data=datas, context=context)
skp_target_yearly_report()