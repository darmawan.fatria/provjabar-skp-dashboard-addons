#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale


class report_skp_target_yearly_report(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(report_skp_target_yearly_report, self).__init__(cr, uid, name, context=context)
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        self.localcontext.update({
            'get_atribut_kepegawaian' : self.get_atribut_kepegawaian,
            'get_skp_yearly_report_raw' : self.get_skp_yearly_report_raw,
            'get_summary_yearly_report_raw' : self.get_summary_yearly_report_raw,
            
            'format_formula_skp':self.format_formula_skp,
            'format_integer' :self.format_integer,
            'format_date_month' :self.format_date_month,
            'get_atribut_jabatan':self.get_atribut_jabatan,
            'get_recap_per_job':self.get_recap_per_job,
            'format_date_id':self.format_date_id,
            
        })
        
    def get_atribut_kepegawaian(self,filters,context=None):
        period_year=filters['form']['yearly_id'][1]
        user_id = False
        data_pegawai=None
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                data_pegawai = result.partner_id
        return data_pegawai;
    def get_atribut_jabatan(self,filters,detail_obj,context=None):
        user_id = False

        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]
        user_pool = self.pool.get('res.users')
        if user_id:
            result = user_pool.browse(self.cr, self.uid, user_id)
            if  result and result.partner_id:
                for job_obj in result.partner_id.job_id_history:
                    print " job_obj.job_id_history : ",job_obj.job_id_history.name
                    print " detail_obj.job_id : ",detail_obj.job_id.name
                    if job_obj.job_id_history.id == detail_obj.job_id.id:
                        print " job_obj.id : ",job_obj.id
                        print " ob_obj.name: ",job_obj.name
                        return job_obj
        return False;
    
    def get_skp_yearly_report_raw(self,filters,detail_obj,context=None):
        period_year=filters['form']['yearly_id'][1]

        user_id = False
        if filters['form']['user_id']:
            user_id=filters['form']['user_id'][0]

        target_pool = self.pool.get('project.project')
        target_ids=target_pool.search(self.cr, self.uid, [('state','in',('confirm','closed')),('target_period_year','=',period_year)
                                        ,('user_id','=',user_id),('job_id','=',detail_obj.job_id.id)]
                                      ,order='target_type_id,name', context=None)
        results = target_pool.browse(self.cr, self.uid, target_ids)
        return results;
    def get_recap_per_job(self,filters,context=None):

        skp_detail_id = filters['form']['skp_job_id'][0]
        skp_yearly_detail_pool = self.pool.get('skp.employee.yearly.history')
        detail_obj = skp_yearly_detail_pool.browse(self.cr,self.uid,skp_detail_id,context=None)

        return detail_obj;
    def get_summary_yearly_report_raw(self,filters,context=None):
        yearly_id = filters['form']['yearly_id'][0]
        skp_yearly_pool = self.pool.get('skp.employee.yearly')
        results = skp_yearly_pool.browse(self.cr, self.uid, [yearly_id,])
        if results:
            return results[0];
        return None
    def format_integer(self,val,context=None):
        try :
            return int(val)
        except:
            return None
    def format_formula_skp(self,recap_yearly):
        if not recap_yearly : return '-'
        try :
            str_jml =str(int( ( recap_yearly.jumlah_perhitungan_skp or 0) + ( recap_yearly.fn_nilai_tambahan or 0) + ( recap_yearly.fn_nilai_kreatifitas or 0) ))
            st_total_skp = str(recap_yearly.jml_skp)
            return "( %s : %s ) = "%(str_jml,st_total_skp)
        except:
            return '-'
    def format_date_month(self,val,context=None):
        try :

            val_date =  datetime.strptime(val,'%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari' :
                fmt_val = val_date.strftime('%d %B')
            else :
                fmt_val = val_date.strftime('%d') +' ' +'Februari';

            return fmt_val
        except:
            return None
    def format_date_id(self,val,context=None):
        try :

            val_date =  datetime.strptime(val,'%Y-%m-%d')
            if val_date.strftime('%B') != 'Pebruari' :
                fmt_val = val_date.strftime('%d %B %Y')
            else :
                fmt_val = val_date.strftime('%d') +' ' +'Februari';

            return fmt_val
        except:
            return None

class wrapped_report_skp_target_yearly(osv.AbstractModel):
    _name = 'report.df_skp_target_yearly_report.report_skp_target_yearly_report'
    _inherit = 'report.abstract_report'
    _template = 'df_skp_target_yearly_report.report_skp_target_yearly_report'
    _wrapped_report_class = report_skp_target_yearly_report
class wrapped_report_skp_target_realisasi_yearly(osv.AbstractModel):
    _name = 'report.df_skp_target_yearly_report.report_skp_target_realisasi_yearly_report'
    _inherit = 'report.abstract_report'
    _template = 'df_skp_target_yearly_report.report_skp_target_realisasi_yearly_report'
    _wrapped_report_class = report_skp_target_yearly_report


