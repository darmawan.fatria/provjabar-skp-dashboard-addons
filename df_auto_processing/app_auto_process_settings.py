from openerp.osv import fields, osv,expression
from mx import DateTime
from openerp import SUPERUSER_ID


class commons_notification(osv.osv_memory):
    _name = "commons.notification"
    _columns = {
        'name': fields.char('Messages', size=128),
    }
commons_notification();
class app_auto_process_settings(osv.osv_memory):
    _name = 'app.auto.process.settings'
    _description = 'Konfigurasi Proses Otomatisi SKP'

    _columns = {
            'name' : fields.char('Konfigurasi Otomatisi SKP', size=64, readonly=True),

    }


    def run_evaluated_process(self, cr, uid, ids, context=None):
        print "running......"
        config_pool = self.pool.get('verification.scheduler')
        now=DateTime.today();
        print "on : ",now
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year=last_month.strftime('%Y')
        target_period_month=last_month.strftime('%m')
        print "period : ",target_period_year," - ",target_period_month

        config_ids = config_pool.search(cr,uid,[],context=None)
        print "total config : ",len(config_ids)

        _DEFAULT_STATE ='evaluated'

        for config_obj in config_pool.browse(cr,uid,config_ids,context=None):
            print "Config ob : ",config_obj.name
            print "Config USER id : ",config_obj.user_id_bkd.name
            user_id_bkd = config_obj.user_id_bkd.id
            for detail_obj in config_obj.detail_ids:


                print "rule name : ",detail_obj.rule_id.name
                company_ids_str =''
                for company_obj in detail_obj.company_ids:
                    company_ids_str = company_ids_str +str(company_obj.id) + ','
                company_ids_str = company_ids_str +'0'
                default_domain = target_period_month,target_period_year,_DEFAULT_STATE,user_id_bkd
                where_clause = """
                                where p.active
                                and p.target_period_month = %s and p.target_period_year = %s and  p.state=%s
                                and p.user_id_bkd = %s
                                and p.company_id in ( """+company_ids_str+ """ )
                                and   """+detail_obj.rule_id.rule+ """
                 """
                if detail_obj.rule_type == 'skp' :
                    query = """
                            select p.id,p.user_id from project_skp p
                            left join project_project t on t.id = p.project_id
                                 """+where_clause+ """
                                and t.state='confirm'
                             """
                    print "skp query ",query
                    cr.execute(query,default_domain)
                    result = cr.fetchall()
                    if result:
                        self.skp_action(cr,user_id_bkd,result)

                if detail_obj.rule_type == 'perilaku' :
                    query = """
                            select p.id,p.user_id from project_perilaku p
                                 """+where_clause+ """
                             """
                    print "perilaku query ",query
                    cr.execute(query,default_domain)
                    result = cr.fetchall()
                    if result:
                        self.perilaku_action(cr,user_id_bkd,result)



        print "end...."

    def skp_action(self,cr,uid,results,context=None):
        skp_pool = self.pool.get('project.skp')
        skp_ids = []
        for skp_id,user_id in results:
            try:
                skp_pool.action_done(cr,uid,[skp_id,],context=None)
            except ValueError:
                print "error for : ",skp_id
            skp_ids.append(skp_id)
        #print "skp ids : ",skp_ids
        #skp_pool.action_done(cr,SUPERUSER_ID,skp_ids,context=None)
    def perilaku_action(self,cr,uid,results,context=None):
        perilaku_pool = self.pool.get('project.perilaku')
        perilaku_ids = []
        for perilaku_id,user_id in results:
            try:
                perilaku_pool.action_done(cr,uid,[perilaku_id,],context=None)
            except ValueError:
                print "error for : ",perilaku_id
            perilaku_ids.append(perilaku_id)

        #print "perilaku ids : ",perilaku_ids
        #perilaku_pool.action_done(cr,SUPERUSER_ID,perilaku_ids,context=None)

    def run_evaluated_process_obj(self, cr, uid, ids, context=None):
        print "running......"
        config_pool = self.pool.get('verification.scheduler')
        skp_pool = self.pool.get('project.skp')
        now=DateTime.today();
        print "on : ",now
        last_month = now + DateTime.RelativeDateTime(months=-1)
        target_period_year=last_month.strftime('%Y')
        target_period_month=last_month.strftime('%m')
        print "period : ",target_period_year," - ",target_period_month

        config_ids = config_pool.search(cr,uid,[],context=None)
        print "total config : ",len(config_ids)

        _DEFAULT_STATE ='evaluated'
        default_domain = []
        default_domain.append(('target_period_year','=',target_period_year))
        default_domain.append(('target_period_month','=',target_period_month))
        default_domain.append(('state','=',_DEFAULT_STATE))

        for config_obj in config_pool.browse(cr,uid,config_ids,context=None):
            print "Config ob : ",config_obj.name
            print "Config USER id : ",config_obj.user_id_bkd.name
            user_id_bkd = config_obj.user_id_bkd.id
            for detail_obj in config_obj.detail_ids:
                perilaku_domain = []
                skp_domain = []

                print "rule name : ",detail_obj.rule_id.name
                company_ids_str =''
                for company_obj in detail_obj.company_ids:
                    company_ids_str = company_ids_str +str(company_obj.id) + ','
                company_ids_str = company_ids_str +'0'

                if detail_obj.rule_type == 'skp' :
                    skp_domain.append(('user_id_bkd','=',user_id_bkd))
                    skp_domain.append(('company_id','in','('+company_ids_str+')'))
                    for domain in default_domain:
                        skp_domain.append(domain)
                    skp_domain.append(detail_obj.rule_id.rule.encode('ascii', 'ignore'))

                    print "domain skp : ",skp_domain
                    skp_ids = skp_pool.search(cr,uid,skp_domain,context=None)
                    print "result : ",skp_ids
                if detail_obj.rule_type == 'perilaku' :

                    print "domain perilaku: ",perilaku_domain



        print "end...."


app_auto_process_settings()