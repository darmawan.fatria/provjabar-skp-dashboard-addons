##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016 <http://->
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "SKP Auto Processing",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "SKP/Setting",
    "description": """
    SKP Auto Processing
    1. Verifikasi Otomatis berdasar kategori
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "df_project",
                "df_skp_employee",
                "df_project_yearly",
                "df_project_multiple_job"
                ],
    'data': [
                "process_rule_config_view.xml",
                "verification_scheduler_view.xml",
                 "app_auto_process_settings_view.xml",
        ],
    
}
