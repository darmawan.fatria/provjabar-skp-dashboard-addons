##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv



RULE_TYPE =[('skp', 'SKP'), ('perilaku', 'Perilaku'),('ambahan', 'Tambahan') ]

class process_rule_config(osv.Model):
	_name = 'process.rule.config'
	_description ='Konfigurasi Rule'

	_columns = {
		'name'     : fields.char('Nama',size=50,required=True),
		'rule'     : fields.text('Rule'),
		'rule_type': fields.selection(RULE_TYPE, 'Tipe',required=True),

		'active' : fields.boolean('Aktif'),

	}



process_rule_config()

