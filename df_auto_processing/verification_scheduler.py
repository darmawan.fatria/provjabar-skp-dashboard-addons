##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from process_rule_config import RULE_TYPE



class verification_scheduler(osv.Model):
	_name = 'verification.scheduler'
	_description ='Proses otomatisasi verifikasi'

	_columns = {
		'name'     : fields.char('Nama',size=50,required=True),
		'user_id_bkd': fields.many2one('res.users', 'Petugas Verifikatur BKD', invisible=True),
		'company_id': fields.many2one('res.company', 'OPD', required=True),
		'detail_ids': fields.one2many('verification.scheduler.detail','config_id', 'Aturan', required=False),
		'active' : fields.boolean('Aktif'),

	}
	_defaults = {
        'active' :True,
		'company_id':lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
		 'user_id_bkd': lambda self, cr, uid, ctx: uid,
	}


verification_scheduler()
class verification_scheduler_detail(osv.Model):
	_name = 'verification.scheduler.detail'
	_description ='Proses otomatisasi verifikasi Detail'

	_columns = {
		'name'     : fields.char('Nama',size=50,required=False),
		'config_id': fields.many2one('verification.scheduler', 'Konfigurasi', required=False),
		'rule_id': fields.many2one('process.rule.config', 'Aturan', required=True),
		'company_ids': fields.many2many('res.company', string='OPD', required=True),
		'rule_type': fields.selection(RULE_TYPE, 'Tipe',required=True),


	}



verification_scheduler_detail()

